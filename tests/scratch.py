from pprint import pprint

def subsets(length, choices) -> list:
    def inner(comb, acc) -> list:
        if (len(comb) == length):
            return acc.append(comb)
        for el in choices:
            comb.append(el)
            inner(comb.copy(), acc)
            comb.pop()
        return acc
    return inner([], [])

pprint(subsets(5, "01"))

