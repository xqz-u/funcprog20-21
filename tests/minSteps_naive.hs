minSteps :: Int -> Int -> Int
-- | Naive implementation of exercise 1.7, traverses the list with a pruned DFS.
-- | Very slow is the double computation of 'nodes' already computed before.
minSteps n g = helper g 0 (g - n)
  where
    helper g cnt minAcc
      | n == g = cnt -- base case
      | (div g 2) < n = cnt + g - n -- shortcut: when halving is too much, you can reach the goal only in g - n steps
      | otherwise = min (allow 3) (min (allow 2) (helper (g - 1) upCnt minAcc))
      where
        upCnt = cnt + 1
        allow x
          | nextVal >= n && mod g x == 0 = helper nextVal upCnt minAcc
          | otherwise = minAcc
          where
            nextVal = div g x
