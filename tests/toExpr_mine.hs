--TODO parsing always ends in parseE' or parseT', signal errors appropriately
--TODO generalize parsing functions?!
import Data.Char
import Data.Maybe

member :: Eq a => a -> [a] -> Maybe a
member x xs
  | null res = Nothing
  | otherwise = Just $ head res
  where
    res = filter (== x) xs

par :: String -> String
-- | Add brackets to an expression
par s = "(" ++ s ++ ")"

type Name = String

data Op
  = Add
  | Sub
  | Mul
  | Div
  | Mod

instance Show Op where
  show Add = "+"
  show Sub = "-"
  show Mul = "*"
  show Div = "/"
  show Mod = "%"

data Expr
  = Val Integer
  | Var Name
  | Op Expr Expr

instance Show Expr where
  show (Val n) = show n
  show (Var x) = x
  -- show (Val n Op op Val m) = par $ show n ++ show op ++ show m
  -- show (p (Op op) q) = par $ show p ++ show op ++ show q
  -- show (exp1 :+: exp2) = par $ show exp1 ++ "+" ++ show exp2
  -- show (exp1 :-: exp2) = par $ show exp1 ++ "-" ++ show exp2
  -- show (exp1 :*: exp2) = par $ show exp1 ++ "*" ++ show exp2
  -- show (exp1 :/: exp2) = par $ show exp1 ++ "/" ++ show exp2
  -- show (exp1 :%: exp2) = par $ show exp1 ++ "%" ++ show exp2
