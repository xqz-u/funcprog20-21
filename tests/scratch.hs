-- ex 2
fact :: Integer -> Integer
fact 0 = 1
fact n
  | n > 0 = n * fact (n - 1)

-- naive implementation, need to memoize those factorials already computed
-- (e.g. to 6! is 5!*6, and 5! was already computed by sumfact)
sumfact_n :: Integer -> Integer
sumfact_n 0 = fact 0
sumfact_n n
  | n > 0 = fact n + sumfact (n - 1)

sumfact :: Integer -> Integer
sumfact 0 = 1
sumfact n
  | n > 0 = inner 1 1 1
  where
    inner :: Integer -> Integer -> Integer -> Integer
    inner curr _ sum
      | curr > n = sum
    inner curr factAcc sum = inner (curr + 1) thisFact (thisFact + sum)
      where
        thisFact = curr * factAcc

pairs :: Integer -> [(Integer, Integer)]
-- | All pairs of integer (i,j) with 1 <= i <= j s.t. i + j + 2*i*j == n
pairs n =
  [ (i, j)
  | i <- [1 .. div (n - 1) 3]
  , j <- [div (n - i) (2 * i + 1)]
  , i + j + 2 * i * j == n
  ]
