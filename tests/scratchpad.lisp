(defun sumfact (n)
  (unless (< n 0)
    (labels ((inner (n acc)
               (if (zerop n)
                   '(1 1)
                   (destructuring-bind (last-fact acc) (inner (1- n) acc)
                     (list (* n last-fact) (+ (* n last-fact) acc))))))
      (second (inner n 0)))))

;;; FIXME not working!!
(defun permute (perm-len choices)
  (labels ((inner (perm acc)
             (format t "~A~%" acc)
             (if (= (length perm) perm-len)
                 perm
                 (dolist (curr choices)
                   (setf acc (append (list (inner (cons curr perm) acc)) acc))))))
    (inner nil nil)))

(defun allow (div g)
  (zerop (mod g div)))

(defun min-steps (n g)
  (format t "~A~%" g)
  (cond ((= n g)
         (progn
           (format t "founddddd!~%")
           0))
        (t (let ((thir (/ g 3))
                 (half (/ g 2))
                 (decr (- g 1)))
             (cond ((< half n)
                    (progn
                      ;; (format t "return (1)!!!~%")
                      (- g n)))
                   ((allow 3 g)
                    (cond ((allow 2 g)
                           (min (1+ (min-steps n thir))
                                (min (1+ (min-steps n half))
                                     (1+ (min-steps n decr)))))
                          (t (min (1+ (min-steps n thir))
                                  (1+ (min-steps n decr))))))
                   ((allow 2 g) (min (1+ (min-steps n half))
                                     (1+ (min-steps n decr))))
                   (t (1+ (min-steps n decr))))))))


(defun upbound (n i)
  (let ((ret (floor (- n i) (1+ (* 2 i)))))
    ;; (format t "n ~A i ~A -> j ~A~%" n i ret)
    ret))


(defun term-test (n i)
  (let ((ret (1+ (* 3 i))))
    ;; (format t "~A >= ~A? ~A~%" ret n (> ret n))
    (> ret n)))


(defun expand (lst n)
  (labels ((pairs ()
             (let ((ret nil))
               (do* ((i 1 (1+ i))
                     (j (upbound n i) (upbound n i)))
                    ((term-test n i) ret)
                 ;; (format t "~A ~A~%" i j)
                 (when (= n (+ i j (* 2 i j)))
                   (push (cons i j) ret))))))
    (let ((res (pairs)))
      (values res (if res
                      (cons n lst)
                      lst)))))
