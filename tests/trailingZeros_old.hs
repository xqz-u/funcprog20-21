toBinary :: Integer -> [Integer]
-- | Convert a number to its binary representation in a list
toBinary n = helper n []
  where
    helper 0 digits = reverse digits -- least significant bit to the left
    helper n digits = helper (div n 2) (mod n 2 : digits)

-- NOTE seemingly, no function Z(n) can be used on the natural numbers...
trailingZeros' :: [Integer]
-- | The series of the number of trailing zeros in the binary representation of
-- | the natural numbers
trailingZeros' = 0 : helper 2
  where
    helper n = nFirstZeros (toBinary n) : (0 : helper (n + 2))
    nFirstZeros = fromIntegral . length . takeWhile (== 0)
