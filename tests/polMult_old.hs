polyAdd :: [Integer] -> [Integer] -> [Integer]
-- | Perform the addition of two polynomials: add the coefficients of the terms
-- | with same degree
polyAdd xs ys
  | lenX < lenY = polyAdd ys xs -- invariant: length xs > length ys
  | otherwise = (take degreeDiff xs) ++ zipWith (+) (drop degreeDiff xs) ys
  where
    (lenX, lenY) = (length xs, length ys)
    degreeDiff = lenX - lenY

monomial :: [Integer] -> Bool
-- | Check if the argument encodes a monomial
monomial (_:xs) = all (== 0) xs

monXmon :: [Integer] -> [Integer] -> [Integer]
-- | Perform monomial multiplication: multiply the coefficients and return a
-- | monomial of degree degree(xss) + degree(yss)
monXmon xss yss
  | monomial xss && monomial yss = head xss * head yss : (tail xss ++ tail yss)
  | otherwise = error "Inputs must be two monomials"

firstPolyTerm :: [Integer] -> [Integer]
-- | Return the first term of a polynomial (a monomial)
firstPolyTerm (x:xs) = x : (replicate . length) xs 0

polyToMon :: [Integer] -> [[Integer]]
-- | Transform a polynomial into a list of the component monomial terms
polyToMon [] = []
polyToMon (0:xs) = polyToMon xs -- skip zero terms
polyToMon xss = firstPolyTerm xss : polyToMon (tail xss)

polMult :: [Integer] -> [Integer] -> [Integer]
-- | Perform polynomial multiplication: transform the arguments into lists of the
-- | composing monomials and exploit the distributive property: x(x+1) = x*x + x*1
polMult xss yss = polyXpoly (polyToMon xss) (polyToMon yss) [0]
  where
    polyXpoly [] _ acc = acc
    polyXpoly (x:xs) yss acc =
      polyXpoly xs yss (polyAdd acc (monXpoly x yss [0]))
      where
        monXpoly _ [] acc = acc
        monXpoly x (y:ys) acc = monXpoly x ys (polyAdd acc (monXmon x y))
