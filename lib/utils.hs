modExp :: Integer -> Integer -> Integer -> Integer
-- | Squared modular exponentiation, returns Integer (b^e) mod m
modExp _ 0 _ = 1
modExp b e m
  | (mod e 2) == 0 = modExp (mod (b * b) m) (div e 2) m
  | otherwise = mod (b * (modExp b (e - 1) m)) m

sqExp :: Integer -> Integer -> Integer
sqExp _ 0 = 1
sqExp b e
  | (mod e 2) == 0 = sqExp (b * b) (div e 2)
  | otherwise = b * sqExp (b * b) (div (e - 1) 2)

-- From FP lab 1, ex 5
modExpSum' :: Integer -> Integer -> Integer
modExpSum' n d = mod (foldr (+) 0 (map (\x -> modExp x x m) [1 .. n])) m
  where
    m = (10 ^ d)

binCombs :: Int -> [[Char]]
binCombs n = helper "" []
  where
    helper :: [Char] -> [[Char]] -> [[Char]]
    helper acc store
      | length acc == n = acc : store
      | otherwise = (helper (acc ++ "0") store) ++ (helper (acc ++ "1") store)

-- NOTE EXAMPLE function composition with multiple arguments
-- λ> ((\x -> x * x) . (\y z -> y + z) 4)
-- ((\x -> x * x) . (\y z -> y + z) 4) :: Num c => c -> c
f a b = ((\x -> x * x) . ((\y z -> y + z) a)) b

-- λ> ((\x y -> x + y) . (\z -> z * z)) 4
-- ((\x y -> x + y) . (\z -> z * z)) 4 :: Num a => a -> a
g a b = ((\x y -> x + y) . (\z -> z * z)) a b

listPrefixes :: [a] -> [[a]]
listPrefixes xss = helper xss 1
  where
    helper :: [a] -> Int -> [[a]]
    helper xss len
      | len == length xss = [xss]
      | otherwise = take len xss : helper xss (len + 1)

-- NOTE the second map returns a list of functions bounded with their arguments,
-- and the first one applies each of them to the function's argument!
incPrefixes :: [a] -> [[a]]
incPrefixes xss = map (\f -> f xss) (map take [1 .. length xss])

decPrefixes :: [a] -> [[a]]
decPrefixes xss = map (\f -> f xss) (map drop [0 .. (length xss) - 1])

subsets :: [a] -> Int -> [[a]]
-- | "Return all possible subsets of length `len' from the `choices' set"
subsets choices n = helper [] 0 []
  where
    helper curr len store
      | len == n = curr : store
      | otherwise =
        foldr (++) [] $ map (\x -> helper (x : curr) (len + 1) store) choices

permute :: Eq a => [a] -> [[a]]
permute [] = [[]]
permute xs = [x : perm | x <- xs, perm <- permute $ delete x xs]
