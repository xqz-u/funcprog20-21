-- NOTE assuming f has a fixpoint, so not checking for error head []
fixpoint :: (Integer -> Integer) -> Integer
-- | Apply f to the natural numbers, and return the smallest fix point for f
-- | (the head of the list of increasing fixed points for f)
fixpoint f = head $ filter (\x -> f x == x) [0 ..]
