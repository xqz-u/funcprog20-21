random :: [Integer]
-- | Construct the infinite list of linear congruential generators in linear time
random = r 2773639
  where
    r n = n : r (mod (25214903917 * n + 11) (2 ^ 45))
