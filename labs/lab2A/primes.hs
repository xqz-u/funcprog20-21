primes :: [Integer]
-- | Infinite lazy list of primes, generated with the Sieve of Sundaram
primes = 2 : [2 * x + 1 | x <- [1 ..], not $ sundaramNum x]

sundaramNum :: Integer -> Bool
-- | Check if a number n can be expressed as n = i + j + 2*1*j with 1<=i<=j,
-- | meaning it has trivial odd factors and is excluded by the sieve of Sundaram
sundaramNum n = findPairs 1
  where
    findPairs i
    --i's upper bound, derived from positive termination condition
      | i > div (n - 1) 3 = False
      | i + j + 2 * i * j == n = True --n has trivial factors
      | otherwise = findPairs $ i + 1 --recur with next i
      where
        j = div (n - i) (2 * i + 1) --compute j for each i using algebra
