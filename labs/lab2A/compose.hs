compose :: [(a -> a)] -> (a -> a)
-- | Fold a list of functions of type (a -> a), starting with the identity
-- | function as first value
compose = foldr (.) id
