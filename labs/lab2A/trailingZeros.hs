trailingZeros :: [Integer]
-- | The series of the number of trailing zeros in the binary representation of
-- | the natural numbers
trailingZeros = series [0] 1 --NOTE starts at 2
  where
    series prev n =
      let this = n : prev --append the numbers occurred before n to n
          update = prev ++ this --update the previous series with the new elements
       in this ++ series update (n + 1) --creating the series
