expand :: [Int] -> Int -> [Int]
-- | Expand a level of the BFS. Given the three operations described in the
-- | exercise, prune the dead ends (values not divisible by 2 or 3) and return a
-- | list of the new, expanded nodes
expand xss t =
  filter
    (> 0) -- eliminated pruned nodes
    [r | x <- xss, r <- [allow x 3 t (div), allow x 2 t (div), x - 1]]
  where
    allow node arg target f
      -- expand a node if it can lead to the goal using the given operation
      -- (second && condition) and if its expansion doesn't lead the exploration
      -- outside of bounds (first && condition)
      | next >= target && mod node arg == 0 = next
      | otherwise = -1 -- else, prune the node
      where
        next = f node arg -- the expansion of a node

minSteps :: Int -> Int -> Int
-- | Perform a search space exploration BFS style, and return the length of the
-- | shortest path from n to g where the only operations allowed are
-- | \x -> x * 2, \x -> x * 3 and \x -> x + 1.
-- | NOTE the search operate backwards from g to n, therefore the inverse of the
-- | three operations is used
minSteps n g = helper [g]
  where
    helper nextNodes
      | elem n nextNodes = 0
      | otherwise = 1 + helper (expand nextNodes n)
