altFib :: Integer -> Integer
-- | Compute the nth alternative Fibonacci number.
altFib n = fibAcc n 0 1
    -- | Helper to compute the nth alternative Fibonacci number linearly,
    -- | which accumulates the current alternative Fibonacci number
  where
    fibAcc 0 acc next = acc
    fibAcc n acc next = fibAcc (n - 1) (((-1) ^ (n + 1)) * next) (acc + next)
