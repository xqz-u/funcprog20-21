prime :: Integer -> Bool
-- | Check if an Integer n is prime
prime n -- 2 and 3 are primes
  | n <= 3 = n > 1
prime n -- Even numbers are not prime
  | mod n 2 == 0 = False
prime n = helper 3 n -- Check divisibility by odd numbers up to m's square root
  where
    helper m n
      | m * m > n = True
      | mod n m == 0 = False
      | otherwise = helper (m + 2) n

modExpSq :: Integer -> Integer -> Integer -> Integer
-- | Squared modular exponentiation, returns Integer (b^e) mod m
modExpSq _ 0 _ = 1
modExpSq b e m
  | mod e 2 == 0 = modExpSq (mod (b * b) m) (div e 2) m
  | otherwise = mod (b * (modExpSq b (e - 1) m)) m

fermatLiar :: Integer -> Integer
-- | Find the first Fermat liar for b, i.e. the first Integer e s.t.
-- |         1 < b < e−1
-- | and
-- |         (b^(e-1)) mod e == 1 && not (prime e)
fermatLiar b
  | b > 1 = helper (b + 1)
  where
    helper e
      | modExpSq b (e - 1) e == 1 && not (prime e) = e
      | otherwise = helper (e + 1)
