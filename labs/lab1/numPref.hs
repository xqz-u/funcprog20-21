numPref :: Int -> Int
-- | Return the number of binary strings of length n where,
-- | for each prefix p, the number of 0s in p is >= than the number of 1s in p
numPref n
  | n <= 20 = helper "0" 1 0 -- Strings starting with 1 don't satisfy the property
  where
    helper acc cnt0 cnt1
      | cnt1 > cnt0 = 0
      | length acc == n = 1
      | otherwise =
        helper (acc ++ "0") (cnt0 + 1) cnt1 +
        helper (acc ++ "1") cnt0 (cnt1 + 1)
