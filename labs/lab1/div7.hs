div7 :: Integer -> Integer
-- | Return how many steps of the recurrence relation defined in the exercise are
-- | needed to check divisibility by 7 on an Integer n.
div7 n
  | n <= 7 || n == 49 = 0
  | otherwise = 1 + div7 ((div n 10) + 5 * (mod n 10))
