modExp :: Integer -> Integer -> Integer -> Integer
-- | Squared modular exponentiation, returns Integer (b^e) mod m
modExp _ 0 _ = 1
modExp b e m
  | mod e 2 == 0 = modExp (mod (b * b) m) (div e 2) m
  | otherwise = mod (b * (modExp b (e - 1) m)) m

modExpSum :: Integer -> Int -> Integer
-- | Compute the sum from k=1 to n of k^k, and return the last d digits of this
-- | term. Use modular exponentiation: (a^a + b^b)mod m = (a^a)mod m + (b^b)mod m
modExpSum n d = helper n (10 ^ d) 0
  where
    helper 0 m acc = mod acc m
    helper n m acc = helper (n - 1) m (acc + modExp n n m)

enlist :: Integer -> [Integer]
-- | Represent an Integer as a list of its digits
enlist n = helper n []
  where
    helper 0 acc = acc
    helper n acc = helper (div n 10) ((mod n 10) : acc)

pad :: Int -> [Integer] -> [Integer]
-- | Fix the result of s = n mod 10^d, where d is the number of end digits
-- | wanted. If length s < length d, then n contained some zeros eliminated by the
-- | modulo, and s must be padded with initial d - (length s) 0s. Otherwise simply
-- | return s
pad d l
  | length l == d = l
  | otherwise = replicate (d - length l) 0 ++ l

lastDigits :: Integer -> Int -> [Integer]
-- | Compute the last d digits of the sequence described in the exercise, save
-- | them into a list and pad with initial 0s if needed
lastDigits n d = pad d ((enlist . modExpSum n) d)
