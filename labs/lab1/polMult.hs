polyAdd :: [Integer] -> [Integer] -> [Integer]
-- | Perform the addition of two polynomials: add the coefficients of the terms
-- | with same degree
polyAdd xs ys
  | lenX < lenY = polyAdd ys xs
  | otherwise = (take degreeDiff xs) ++ zipWith (+) (drop degreeDiff xs) ys
  where
    (lenX, lenY) = (length xs, length ys)
    degreeDiff = lenX - lenY

polyAddVars :: [[Integer]] -> [Integer]
-- | Add a variable number of polynomials, represented as a list of polynomials
polyAddVars xs = foldr polyAdd [0] xs

monomial :: [Integer] -> Bool
-- | Check if the argument encodes a monomial
monomial (_:xs) = all (== 0) xs

monXmon :: [Integer] -> [Integer] -> [Integer]
-- | Perform monomial multiplication: multiply the coefficients and return a
-- | monomial of degree degree(xss) + degree(yss)
monXmon a b
  | monomial a && monomial b = head a * head b : (tail a ++ tail b)

firstPolyTerm :: [Integer] -> [Integer]
-- | Return the first term of a polynomial (a monomial)
firstPolyTerm (x:xs) = x : (replicate . length) xs 0

decPrefixes :: [a] -> [[a]]
-- | Return a list of decreasing prefixes in a list of any type
-- | e.g. [1,2,3] -> [[1,2,3], [1,2],[3]], by mapping the partially applied
-- | functions returned by the second map to xs
decPrefixes xs = map (\f -> f xs) . map drop $ [0 .. length xs - 1]

polyToMon :: [Integer] -> [[Integer]]
-- | Transform a polynomial into a list of its component monomials, skipping zero
-- | entries
polyToMon xs = filter (\y -> head y /= 0) . map firstPolyTerm $ decPrefixes xs

polMult :: [Integer] -> [Integer] -> [Integer]
-- | Perform polynomial multiplication: decompose the arguments into their
-- | monomial components and exploit the distributive property: x(x+1) = x*x + x*1
polMult xs ys =
  polyAddVars . map (\x -> monXpoly x $ polyToMon ys) $ polyToMon xs
  where
    monXpoly x ys = polyAddVars . map (\y -> monXmon x y) $ ys
