import Clause
import Parser
import Types

process :: String -> Clauses
process str = programToClauses . parseProgram $ str
