module Clause
  ( programToClauses
  ) where

import Types

programToClauses :: Program -> Clauses
-- | Convert a Program (a list of statements with their line numbers) to a list
-- | of Clause (a disjunction of Facts or Rules)
programToClauses (Program statements) = toClauses statements []

toClauses :: [(Statement, Int)] -> Clauses -> Clauses
-- | Tail recursive helper for `programToClauses`
toClauses [] acc = reverse acc
toClauses ((Fact args, _):xs) acc = toClauses xs ([(args, True)] : acc)
toClauses ((Rule conclusion premises, _):xs) acc =
  toClauses
    xs
    ((concat [[(fApp, False)] | fApp <- premises] ++ [(conclusion, True)]) : acc)
toClauses (_:xs) acc = toClauses xs acc
