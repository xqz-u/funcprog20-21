import Analysis
import Parser
import System.Environment
import System.IO
import Types

process :: String -> Maybe Program
process str = (analyse . parseProgram) str

main = do
  args <- getArgs
  let reader =
        if args == []
          then getContents
          else readFile (head args)
  text <- reader
  print (process text)
