module Resolution
  ( resolveClauses
  ) where

import Clause
import Data.List
import Data.Maybe (fromJust)
import Parser
import Types
import Unify

type Table = [(String, Int)]

type Memory = [(Argument, Argument)]

-- TODO last layer of unification on clauses
test = resolveClauses . programToClauses . parseProgram

permute :: Eq a => [a] -> [[a]]
permute [] = [[]]
permute xs = [x : perm | x <- xs, perm <- permute $ delete x xs]

resolveClauses :: Clauses -> Clauses
resolveClauses clauses = sortOn length . folResolve (stdClauses clauses) $ []

folResolve :: Clauses -> Clauses -> Clauses
folResolve clauses inferences
  | and [x `elem` clauses | x <- inferences'] = clauses'
  | otherwise = folResolve clauses' inferences'
  where
    inferences' = setUnion inferences (infer clauses)
    clauses' = setUnion clauses inferences'
    setUnion :: Clauses -> Clauses -> Clauses
    setUnion = unionBy (\x y -> y `elem` (permute x))

infer :: Clauses -> Clauses
infer [] = []
infer (x:xs) = concatMap (resolve x) xs ++ infer xs

resolve :: Clause -> Clause -> Clauses
resolve xs ys = concatMap helper xs
  where
    helper x =
      uncurry
        (zipWith
           (\theta x' ->
              folFactor
                [ (applyUnifier theta fn, v)
                | (fn, v) <- [z | z <- xs ++ ys, not (z == x || z == x')]
                ]))
        (unzip $ unifyClause x ys)

folFactor :: Clause -> Clause
folFactor = nubBy (\(f0, v0) (f1, v1) -> v0 == v1 && mgu f0 f1 /= Nothing)

unifyClause ::
     (FuncApplication, Bool) -> Clause -> [(Unifier, (FuncApplication, Bool))]
unifyClause (x, v) ys =
  [ (fromJust u, (y, v'))
  | (y, v') <- ys
  , v == not v'
  , let u = mgu x y
  , u /= Nothing
  ]

stdClauses :: Clauses -> Clauses
stdClauses =
  fst .
  foldr
    (\clause (clauses, table) ->
       let (clause', table') = stdClause clause table
        in (clause' : clauses, table'))
    ([], [])

stdClause :: Clause -> Table -> (Clause, Table)
stdClause statements table = (statements', table')
  where
    (fns, vals) = unzip statements
    (fnames, fargs) =
      foldr (\(FuncApp x y) (xs, ys) -> (x : xs, y : ys)) ([], []) fns
    (fargs', table') = stdArgsList fargs table
    statements' =
      zip (zipWith (\name args -> FuncApp name args) fnames fargs') vals

stdArgsList :: [[Argument]] -> Table -> ([[Argument]], Table)
stdArgsList xss table =
  let (xss', table', _) = foldr helper ([], table, []) xss
   in (xss', table')
  where
    helper x (xs, tbl, mem) =
      let (x', tbl', mem') = stdArgs x tbl mem
       in (x' : xs, tbl', mem')

stdArgs :: [Argument] -> Table -> Memory -> ([Argument], Table, Memory)
stdArgs args table processed = foldr helper ([], table, processed) args
  where
    helper x (xs, tbl, mem)
      | v == Nothing =
        let (x', tbl') = stdArg x tbl
         in (x' : xs, tbl', ((x, x') : mem))
      | otherwise = (fromJust v : xs, tbl, mem)
      where
        v = lookup x mem

stdArg :: Argument -> Table -> (Argument, Table)
stdArg (Arg var) table
  | v == Nothing = (Arg $ var ++ "0", (var, 0) : table)
  | otherwise = (Arg $ var ++ show (n + 1), newTable)
  where
    v = lookup var table
    n = fromJust v
    newTable = (var, n + 1) : (delete (var, n) table)
stdArg c table = (c, table)
-- reducibleTerm :: (FuncApplication, Bool) -> (FuncApplication, Bool) -> Bool
-- reducibleTerm ((FuncApp f0 fargs0), v0) ((FuncApp f1 fargs1), v1)
--   | not . and $ [f0 == f1, length fargs0 == length fargs1, v0 == v1] = False
-- reducibleTerm ((FuncApp _ fargs0), _) ((FuncApp _ fargs1), _) =
--   and . map (uncurry sameArgs) $ zip fargs0 fargs1
--   where
--     sameArgs (Const c0) (Const c1) = c0 == c1
--     sameArgs _ _ = True
-- applyFactoring ::
-- factor :: Clause -> Clause
-- factor = snd . foldr applyFactoring ([], [])
-- applyFactoring ::
--      (FuncApplication, Bool) -> (Clause, Clause) -> (Clause, Clause)
-- applyFactoring fapp ([], acc) = ([fapp], fapp : acc)
-- applyFactoring (f, v) (f', acc)
--   | v /= v' || u == Nothing = (f', (f, v) : acc) --cannot unify
--   | mgf' == mgf = (f', acc) --unification with a Const, drop superfluous fapp
--   --unification with a Var, update most general fapp
--   | otherwise = ([(mgf', v')], (mgf', v') : acc)
--   where
--     (mgf, v') = head f'
--     u = mgu f mgf
--     mgf' = applyUnifier (fromJust u) mgf
