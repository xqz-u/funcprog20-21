-- TODO REWRITE (keep using `fold` idea!!!)
module Unify
  ( applyUnifier
  , mgu
  ) where

import Types

mgu :: FuncApplication -> FuncApplication -> Maybe Unifier
-- | Find the MGU between two FuncApplications, if it exists
mgu (FuncApp fname1 args1) (FuncApp fname2 args2)
  -- cannot unify predicates with different names or arities
  | (not . and) [fname1 == fname2, length args1 == length args2] = Nothing
mgu (FuncApp fname1 args1) (FuncApp fname2 args2) =
  mguHelper (args1, args2) args1 args2 1 []

mguHelper ::
     ([Argument], [Argument])
  -> [Argument]
  -> [Argument]
  -> Int
  -> Unifier
  -> Maybe Unifier
-- | Helper function to find the MGU of two FuncApplications; only constant and
-- | variable unification is considered - not predicate
mguHelper (argsA, argsB) [] [] _ u --no more substitutions possible
  | and $ zipWith (==) [argsA, argsB] [unifyArguments u x | x <- [argsA, argsB]] =
    Just $ reverse u --positive base case
mguHelper _ [] [] _ _ = Nothing --negative base case
mguHelper _ ((Const c1):xs) ((Const c2):ys) i u
  | c1 /= c2 = Nothing --negative case, different ground terms
mguHelper args ((Const c1):xs) ((Const c2):ys) i u =
  mguHelper args xs ys (i + 1) u --same ground terms, keep substituting
-- Either argument list has a variable: update the unifier, apply it to the
-- FuncApp's arguments and recur
mguHelper args ((Arg var):xs) (y:ys) i u = mguRecursion (var, y) args i u
mguHelper args (x:xs) ((Arg var):ys) i u = mguRecursion (var, x) args i u

mguRecursion ::
     Substitution -> ([Argument], [Argument]) -> Int -> Unifier -> Maybe Unifier
-- | Mutually recursive helper for `mguHelper`, which Adds a new substitution to
-- | the partially built unifier, and applies it to the arguments of two
-- | predicates
mguRecursion subst (args1, args2) i u =
  let u' = addToUnifier subst u
      (args1', args2') = (unifyArguments u' args1, unifyArguments u' args2)
      (rest1, rest2) = (drop i args1', drop i args2')
   in mguHelper (args1', args2') rest1 rest2 (i + 1) u'

addToUnifier :: Substitution -> Unifier -> Unifier
-- | Add a Substitution to a Unifier, applying chains if they are present (i.e.
-- | when x = {A/N} and y = {N/X}, then {A/X}, where x is an old
-- | substitution in the Unifier and y is the substitution to be added)
addToUnifier (name, _) u
  | lookup name u /= Nothing = u --a substitution for `name` is already defined
addToUnifier subst u = subst : [chain subst s | s <- u]
  where
    chain (varName, (Arg newVar)) (name, typedValue) =
      (name, replaceArg (varName, (Arg newVar)) typedValue)
    chain _ s = s --Const: chaining a ground term makes the unifier less general

applyUnifier :: Unifier -> FuncApplication -> FuncApplication
-- | Apply the substitutions defined in a Unifier to a FuncApplication's
-- | arguments
applyUnifier u (FuncApp fname args) = FuncApp fname (unifyArguments u args)

unifyArguments :: Unifier -> [Argument] -> [Argument]
-- | Apply the substitutions defined in a Unifier to a list of Arguments
unifyArguments u args = foldl (flip replace) args u

replace :: Substitution -> [Argument] -> [Argument]
-- | Apply a substitution to a list of Arguments
replace subst args = [replaceArg subst x | x <- args]

replaceArg :: Substitution -> Argument -> Argument
-- | Apply a substitution to a single Argument. A substitution is valid only for
-- | an Arg (variable); a Const (ground term) cannot be substituted
replaceArg (name, typedValue) (Arg var)
  | name == var = typedValue --match, apply substitution
replaceArg _ x = x --not match, just return original Argument
