module Query
  ( answerQueries
  ) where

import Clause
import Data.List (intercalate, nub, sort)
import Data.Maybe (fromJust)
import Resolution
import Types
import Unify

extractQueries :: Program -> [FuncApplication]
-- | Return a list of negated queries that a knowledge base will be inquired with
extractQueries (Program lines) = queries lines
  where
    queries [] = []
    queries ((Query q, _):xs) = q : queries xs
    queries (_:xs) = queries xs

answerQueries :: Program -> String
-- | Return a string of queries and their answers, formatted as per specification
answerQueries program =
  intercalate "\n" [(print q) ++ answer q resClosure | q <- queries] ++ "\n"
  where
    (kb, queries) = (programToClauses program, extractQueries program)
    resClosure = resolveClauses kb --compute the resolution closure of kb
    print query = show query ++ ": " --format a query as specified

answer :: FuncApplication -> Clauses -> String
-- | Evaluate a query on the resolution closure of a knowledge base
answer (FuncApp query args) kb
  --the query is only about facts, not relations
  | isConstantQuery && [(FuncApp query args, True)] `elem` kb = "yes"
  | isConstantQuery = "no"
  where
    isConstantQuery = args == [Const c | (Const c) <- args]
--when we inquire relations, call dedicated function to handle printing
answer query kb = formatQueryVars query ++ "[" ++ commatizeAnswers ++ "]"
  where
    commatizeAnswers = (intercalate "," . sort) $ extractAnswers query kb

extractAnswers :: FuncApplication -> Clauses -> [String]
-- | Format a list of unifications that satisfy a query given the resolution
-- | closure of a knowledge base, as specified in the assignment
extractAnswers (FuncApp q qargs) kb =
  [ unifications inference
  | [inference] <- kb --an answer always has length one
  , answersQuery (FuncApp q qargs) inference
  ]
  where
    unifications (FuncApp _ args, _)
      --in a query mixing literals and variables e.g. f(a,X), return a list of
      --substitutions satisfying the variables only (i.e. substitutions for X)
     = parenthesizeList [x | x <- args, x `notElem` qargs]

answersQuery :: FuncApplication -> (FuncApplication, Bool) -> Bool
-- | Check if a positive predicate from the resolution closure of a knowledge
-- | base and a query satisfies the latter
answersQuery query (predicate, True) =
  theta /= Nothing && predicate == applyUnifier (fromJust theta) query
  where
    theta = mgu query predicate --try to unify the query with a candidate answer,
                                -- then check if they are equal
answersQuery _ _ = False

formatQueryVars :: FuncApplication -> String
-- | Format the variables of a query on predicates of a knowledge base as per
-- | assignment specification
formatQueryVars (FuncApp _ qargs) =
  parenthesizeList (nub $ variables qargs) ++ "<-"
  where
    variables args = [(Arg v) | (Arg v) <- args]

parenthesizeList :: Show a => [a] -> String
-- | Format a list of showable objects with parentheses instead of brackets
parenthesizeList [x] = show x
parenthesizeList xs = "(" ++ (tail . init $ show xs) ++ ")"
