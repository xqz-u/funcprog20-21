resolveClauses [[(FuncApp "child" [Arg "B", Const "john"], False), (FuncApp "father" [Const "john", Arg "B"], True)], [(FuncApp "child" [Const "sue", Const "john"], True)] ]

output list should include [(father(john,sue),True)]

----------

resolveClauses [[(FuncApp "mother" [Const "jane", Const "sue"], True)], [(FuncApp "alive" [Const "jane"], True)], [(FuncApp "mother" [Arg "X", Arg "Y"], False),(FuncApp "parent" [Arg "X", Arg "Y"], True)],[(FuncApp "parent" [Arg "X", Arg "Y"], False), (FuncApp "alive" [Arg "X"], False), (FuncApp "older" [Arg "X", Arg "Y"], True)]]

output list should include [(parent(jane,sue),True)] and [(older(jane,sue),True)]

----------

resolveClauses [[(FuncApp "at" [Arg "X",Const "bob"],False), (FuncApp "at" [Arg "X",Const "carol"],True)],[(FuncApp "at" [Const "dance", Const "bob"],True)],[(FuncApp "at" [Arg "Y", Const "carol"], False),(FuncApp "answer" [Arg "Y"],True)]]

output list should include [(answer(dance),True)]

----------
