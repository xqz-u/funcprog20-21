module Lexer where

--library imports
import Data.Char

--user level imports
import Error

data LexToken
  = DotTok
  | CommaTok
  | FollowsTok
  | QueryTok
  | LparTok
  | RparTok
  | IdentTok String
  | VarTok String
  deriving (Eq)

instance Show LexToken where
  show DotTok = "."
  show CommaTok = ","
  show FollowsTok = ":-"
  show QueryTok = "?-"
  show LparTok = "("
  show RparTok = ")"
  show (IdentTok name) = "<id:" ++ name ++ ">"
  show (VarTok name) = "<var:" ++ name ++ ">"

lexer :: String -> [(LexToken, Int)]
-- | Accept the input alphabet and return a list of LexTokens along with their
-- | line number occurrence in `string`
lexer string = helper 1 string
  where
    helper _ [] = [] --done tokenizing
    helper linenr (c:cs)
      | c == '.' = collect DotTok cs
      | c == ',' = collect CommaTok cs
      | c == ':' && (head cs) == '-' = collect FollowsTok (tail cs) --skip 2chars
      | c == '?' && (head cs) == '-' = collect QueryTok (tail cs) --same here
      | c == '(' = collect LparTok cs
      | c == ')' = collect RparTok cs
      | c == '\n' = helper (linenr + 1) cs --newline: increase line number
      | c == '%' = helper linenr (dropWhile (/= '\n') cs) --skip comments
      | elem c " \t " = helper linenr cs --skip unprintable characters
      | isAlpha c =
        collect
          ((if isUpper c
              then VarTok
              else IdentTok) $
           c : takeWhile pred cs)
          (dropWhile pred cs) --coerce to correct tok type & consume identifier
      where
        collect tok rest = (tok, linenr) : helper linenr rest --save tok & recur
        pred = isAlphaNum --default predicate defining identifiers
    helper linenr (c:_) = Error.lexError linenr c --signal illegal token
