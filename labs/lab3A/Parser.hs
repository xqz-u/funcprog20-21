-- TODO comment
module Parser
  ( parseProgram
  ) where

import Data.Char

import Error
import Lexer
import Types

--function signatures
parseProgram :: String -> Program
parseProlog :: [(Statement, Int)] -> [(LexToken, Int)] -> [(Statement, Int)]
parseStatement :: [(LexToken, Int)] -> (Statement, [(LexToken, Int)])
parseStatement' :: [(LexToken, Int)] -> ([FuncApplication], [(LexToken, Int)])
parseRelationList :: [(LexToken, Int)] -> ([FuncApplication], [(LexToken, Int)])
parseRelationList' ::
     [(LexToken, Int)] -> ([FuncApplication], [(LexToken, Int)])
parseRelation :: [(LexToken, Int)] -> (FuncApplication, [(LexToken, Int)])
parseArgs :: [(LexToken, Int)] -> ([Argument], [(LexToken, Int)])
parseArgList :: [(LexToken, Int)] -> ([Argument], [(LexToken, Int)])
parseArgList' :: [(LexToken, Int)] -> ([Argument], [(LexToken, Int)])
parseArgument :: [(LexToken, Int)] -> (Argument, [(LexToken, Int)])
-- | Entry point of the parser
parseProgram input = Program $ parseProlog [] (lexer input)

-- | Prolog -> Statement Prolog
-- | Prolog -> <empty>
parseProlog accepted [] = reverse accepted
parseProlog accepted tokens = parseProlog ((statement, linenr) : accepted) rs
  where
    linenr = snd . head $ tokens
    (statement, rs) = parseStatement tokens

-- | Statement -> '?-' Relation '.'
-- | Statement -> Relation Statement'
parseStatement (t:ts)
  | isQuery =
    if (fst . head $ rs) == DotTok
      then (Query conclusion, tail rs)
      else eofError --ex: ?- Query EOF
  | null premises = (Fact conclusion, fs)
  | otherwise = (Rule conclusion premises, fs)
  where
    isQuery = fst t == QueryTok
    (conclusion, rs) = selector isQuery parseRelation (t : ts)
    (premises, fs) = selector isQuery parseStatement' rs
    selector True fn ls = fn $ tail ls
    selector _ fn ls = fn ls

-- | Statement' -> '.'
-- | Statement' -> ':-' RelationList '.'
parseStatement' [] = eofError --ex: Fact EOF
parseStatement' (t:ts)
  | fst t == DotTok = ([], ts)
  | fst t == FollowsTok =
    if fst r == DotTok
      then (relations, rs)
      else eofError -- ex: Rule EOF
  where
    (relations, (r:rs)) = parseRelationList ts

-- | RelationList -> Relation RelationList'
parseRelationList [] = eofError --ex: Relation :- EOF
parseRelationList tokens = (arg : args, fs)
  where
    (arg, rs) = parseRelation tokens
    (args, fs) = parseRelationList' rs

-- | RelationList' -> ',' Relation RelationList'
-- | RelationList' -> <empty>
parseRelationList' [] = eofError --ex: Relation0, Relation1 EOF
parseRelationList' tokens
  | fst t == CommaTok = (arg : args, fs)
  | otherwise = ([], tokens) --construct the whole list NOTE not tail recursive
  where
    (t:ts) = tokens
    (arg, rs) = parseRelation ts
    (args, fs) = parseRelationList' rs

-- | Relation -> Identifier Args
parseRelation [] = eofError -- ex: ?- EOF
parseRelation (t:ts)
  | earlyError $ fst t = expectedError (snd t) identifierError
  | otherwise = (FuncApp (isIdentifier $ arg) args, fs)
  where
    earlyError (VarTok x) = False
    earlyError (IdentTok x) = False
    earlyError _ = True
    (arg, rs) = parseArgument (t : ts)
    (args, fs) = parseArgs rs
    isIdentifier (Const x) = x
    isIdentifier _ = expectedError (snd t) identifierError
    identifierError =
      "a relation name (i.e. an identifier that start with a lower case letter)"

-- | Args -> '(' ArgList ')'
-- | NOTE a missing RparTok is not considered here, as it will be intercepted
-- | down the parsing line; this function shall just consume it
parseArgs [] = eofError --ex: Identifier EOF
parseArgs (t:ts)
  | fst t /= LparTok = expectedError (snd t) "an opening parenthesis"
  | otherwise = (args, rs) --return valid Argument list
  where
    (args, (_:rs)) = parseArgList ts

-- | ArgList -> Argument ArgList'
parseArgList [] = eofError --ex: Identifier(EOF
parseArgList tokens = (arg : args, fs)
  where
    (arg, rs) = parseArgument tokens
    (args, fs) = parseArgList' rs

-- | ArgList' -> ',' Argument ArgList'
-- | ArgList' -> <empty>
parseArgList' [] = eofError --ex: Identifier(Argument, Argument..EOF
parseArgList' tokens
  | fst t == CommaTok = (arg : args, fs)
  | otherwise = ([], tokens) --construct the whole list NOTE not tail recursive
  where
    (t:ts) = tokens
    (arg, rs) = parseArgument ts
    (args, fs) = parseArgList' rs

-- | Argument -> <variable> | <constant>
parseArgument [] = eofError --ex: Identifier(Argument..,EOF
parseArgument (t:ts) = matcher $ fst t
  where
    matcher (VarTok x) = (Arg x, ts)
    matcher (IdentTok x) = (Const x, ts)
    matcher _ = expectedError (snd t) "a literal"
