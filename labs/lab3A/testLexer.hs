import Lexer
import System.Environment
import System.IO

main = do
  args <- getArgs
  let reader =
        if args == []
          then getContents
          else readFile (head args)
  text <- reader
  print (lexer text)
