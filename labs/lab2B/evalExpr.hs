import Data.Char --`isDigit`, `isAlpha`
import Data.List --`nub` (unique values in list)
import Data.Maybe --`fromJust`

type Name = String

type Valuation = [(Name, Integer)]

data Expr
  = Val Integer
  | Var Name
  | Expr :+: Expr
  | Expr :-: Expr
  | Expr :*: Expr
  | Expr :/: Expr
  | Expr :%: Expr

par :: String -> String
-- | Wrap an expression in brackets
par s = "(" ++ s ++ ")"

instance Show Expr where
  show (Val n) = show n
  show (Var x) = x
  show (exp1 :+: exp2) = par $ show exp1 ++ "+" ++ show exp2
  show (exp1 :-: exp2) = par $ show exp1 ++ "-" ++ show exp2
  show (exp1 :*: exp2) = par $ show exp1 ++ "*" ++ show exp2
  show (exp1 :/: exp2) = par $ show exp1 ++ "/" ++ show exp2
  show (exp1 :%: exp2) = par $ show exp1 ++ "%" ++ show exp2

lexer :: String -> [String]
-- | Simple lexer, accepts the grammar's alphabet and tokenizes on whitespace
lexer [] = [] --done tokenizing
lexer (c:cs)
  | elem c "\n\t " = lexer cs --skip unprintable characters
  | elem c "()%*/+-" = [c] : (lexer cs) --save alphabet tokens
  | isAlpha c = consume isAlpha --save Var
  | isDigit c = consume isDigit --save Val
  | otherwise = error "Syntax Error: invalid character in input"
  where
    consume pred = (c : takeWhile pred cs) : lexer (dropWhile pred cs)

toExpr :: String -> Expr
-- | Parse the string encoding an expression into an Expr data type
toExpr s = fst . parseE $ lexer s

parseE :: [String] -> (Expr, [String])
-- | E -> T E'
parseE tokens = parseE' acc rest
  where
    (acc, rest) = parseT tokens

parseE' :: Expr -> [String] -> (Expr, [String])
-- | E' -> + T E' | - T E' | Empty
parseE' accepted tokens
  | null tokens = (accepted, []) --parsing done, can't be matched by (t:ts)
  | t == "+" = parseE' (accepted :+: acc) rest
  | t == "-" = parseE' (accepted :-: acc) rest
  | otherwise = (accepted, tokens) --empty Expr
  where
    (acc, rest) = parseT ts
    (t:ts) = tokens

parseT :: [String] -> (Expr, [String])
-- | T -> F T'
parseT tokens = parseT' acc rest
  where
    (acc, rest) = parseF tokens

parseT' :: Expr -> [String] -> (Expr, [String])
-- | T' -> * F T' | / F T' | % F T' | Empty
parseT' accepted tokens
  | null tokens = (accepted, []) --parsing done, can't be matched by (t:ts)
  | t == "*" = parseT' (accepted :*: acc) rest
  | t == "/" = parseT' (accepted :/: acc) rest
  | t == "%" = parseT' (accepted :%: acc) rest
  | otherwise = (accepted, tokens) --empty Expr, parse lower priority operators
  where
    (acc, rest) = parseF ts
    (t:ts) = tokens

parseF :: [String] -> (Expr, [String])
-- | F -> (E) | <integer> | <identifier>
parseF tokens
  | null tokens = error "Malformed Expr! Cannot parse" --minimal error handling
  | isAlpha $ nextTok = (Var $ completeToken isAlpha, ts)
  | isDigit $ nextTok = (Val $ readInteger $ completeToken isDigit, ts)
  | t == "(" && c == ")" = (expr, cs)
  where
    (t:ts) = tokens
    nextTok = head t
    (expr, (c:cs)) = parseE ts --recur from starting symbol when parsing (E)
    completeToken pred = takeWhile pred t
    readInteger s = read s :: Integer --one way to coerce strings to other types

vars :: Expr -> [Name]
-- | Return the sorted list of unique identifiers in `expression'
vars expression = sort . nub $ helper expression
  where
    helper (Var x) = [x]
    helper (exp1 :+: exp2) = helper exp1 ++ helper exp2
    helper (exp1 :-: exp2) = helper exp1 ++ helper exp2
    helper (exp1 :*: exp2) = helper exp1 ++ helper exp2
    helper (exp1 :/: exp2) = helper exp1 ++ helper exp2
    helper (exp1 :%: exp2) = helper exp1 ++ helper exp2
    helper _ = [] --Vals do not contain variables

operands :: Expr -> Expr -> Valuation -> (Maybe Integer, Maybe Integer)
-- | Helper function to wrap the evaluations of two Expr in a tuple
operands e1 e2 vals = (evalExpr e1 vals, evalExpr e2 vals)

handle ::
     Expr
  -> Expr
  -> Valuation
  -> (Integer -> Integer -> Integer)
  -> Maybe Integer
  -- | Helper function which evaluates a complex expression p = e1 op e2, where
  -- | op = {:+:,:-:,:*:,:/:,:%:}. It checks for impossible operations (division
  -- | by 0) and missing valuations, and returns the evaluation of p under vals
handle e1 e2 vals (div)
  | snd (operands e1 e2 vals) == Just 0 = Nothing --division by 0 is impossible
handle e1 e2 vals fn
  --either not enough substitutions variable/value, or division by 0 somewhere
  --down the evaluation line of e1 and e2
  | o1 == Nothing || o2 == Nothing = Nothing --NOTE any isNothing (o1,o2) fails
  --evaluation possible: apply fn to the Integer valuations of o1 and o2
  | otherwise = Just $ (\x y -> fn x y) (fromJust o1) (fromJust o2)
  where
    (o1, o2) = operands e1 e2 vals --the Maybe Integer values of e1 and e2

evalExpr :: Expr -> Valuation -> Maybe Integer
-- | Evaluate an expression with a given Valuation (if possible)
evalExpr (Val x) _ = Just x --numbers do not need substitution
--substitute v with its numerical valuation (if any)
evalExpr (Var v) vals = lookup v vals
--evaluate the sub expressions individually, then combine their result with the
--right operator
evalExpr (p :+: q) vals = handle p q vals (+)
evalExpr (p :-: q) vals = handle p q vals (-)
evalExpr (p :*: q) vals = handle p q vals (*)
evalExpr (p :/: q) vals = handle p q vals (div)
evalExpr (p :%: q) vals = handle p q vals (rem)
