-- ex 1 -> TYPES
-- zip :: [a] -> [b] -> [(a, b)]
-- concat :: [[a]] -> [a]
-- uncurry :: (a -> b -> c) -> ((a, b) -> c)
-- plus1 :: Num p => [p] -> [p]
f :: [Integer] -> Integer
f = sum . h . g

g :: [a] -> (a, a)
g = (\x -> (head x, (head . reverse) x))

h :: (a, a) -> [a]
h (x, y) = [x, y]

-- ex 2 -> PROGRAMMING QUESTION
digits :: Integer -> [Integer]
digits n = helper n []
  where
    helper 0 acc = acc
    helper n acc = helper (n `div` 10) ((n `mod` 10) : acc)

isDCSN :: Integer -> Bool
isDCSN n
  | length ds /= 9 = False
  | otherwise = (sum $ zipWith (*) ds factors) `mod` 11 == 0
  where
    ds = digits n
    factors = reverse $ -1 : [2 .. 9]

-- ex 3 -> LIST COMPREHENSIONS
-- a
relPrimePairs n = [(i, j) | i <- [2 ..], j <- [i + 1 .. n], gcd i j == 1]

--b
suits = ["Clubs", "Diamonds", "Hearts", "Spades"]

cards = map show [2 .. 10]

honours = ["J", "Q", "K", "A"]

deck = [(x, y) | x <- suits, y <- cards ++ honours]

--c
locations n xs = [y | (x, y) <- zip xs [0 .. length xs], x == n]

--ex 4 -> INFINITE LISTS
--a
iterate' :: (a -> a) -> a -> [a]
iterate' fn elt = helper [fn elt]
  where
    helper (x:xs) = x : helper (fn x : xs)

--cleaner
iterate'' :: (a -> a) -> a -> [a]
iterate'' fn elt = fn elt : iterate'' fn (fn elt)

--b --FROM SOLUTIONS
ints :: [Integer]
ints = 0 : merge 1
  where
    merge n = n : -n : merge (n + 1)

--c --FROM SOLUTIONS
primes = sieve [2 ..]
  where
    sieve (p:xs) = p : sieve [x | x <- xs, x `mod` p /= 0]

union (x:xs) (y:ys)
  | x < y = x : union xs (y : ys)
  | y < x = y : union (x : xs) ys
  | otherwise = x : union xs ys

composites = foldr fuse [] (map multiples primes)
  where
    multiples p = [n * p | n <- [2 ..]]
    fuse (x:xs) ys = x : (union xs ys)

--ex 5 -> ADT
data RPN
  = Value Integer
  | Plus
  | Minus
  | Times
  | Div

rpn :: [RPN] -> Integer
rpn xs = helper xs []
  where
    helper [] (res:[]) = res
    helper ((Value v):xs) stack = helper xs (v : stack)
    helper (Plus:xs) (n:m:ys) = helper xs (m + n : ys)
    helper (Minus:xs) (n:m:ys) = helper xs (m - n : ys)
    helper (Times:xs) (n:m:ys) = helper xs (m * n : ys)
    helper (Div:xs) (n:m:ys) = helper xs (m `div` n : ys)

--ex 6 -> MODULES
-- module Fifo (Fifo, empty, isEmpty, insert, top, remove) where
newtype Fifo a =
  F [a]

empty :: Fifo a
empty = F []

isEmpty :: Fifo a -> Bool
isEmpty (F l) = null l

insert :: a -> Fifo a -> Fifo a
insert x (F l) = F (l ++ [x])

top :: Fifo a -> a
top (F (x:xs)) = x

remove :: Fifo a -> Fifo a
remove (F (x:xs)) = F xs
