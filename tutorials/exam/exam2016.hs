--ex 1 -> TYPES
--a
--(Int, [Int], [[Int]])
--b
-- [Char] -> [Char]
--c
g :: a -> (a -> (a, a))
g = (\x -> (\y -> (y, x)))

--d
-- (a -> b -> b) -> b -> [a] -> b
--e
h :: (Char -> Int) -> Bool
h = (\f -> map f "Text" == [1, 2, 3, 4])

--ex 2 -> PROGRAMMING IN HASKELL
--ex 3 -> HOF
--a
isEqual :: Eq b => (a -> b) -> (a -> b) -> [a] -> Bool
isEqual f g l = and $ zipWith (==) (map f l) (map g l)

--cleaner
isEqual' f g l = (map f l) == (map g l)

--b
concat' :: [[a]] -> [a]
concat' = foldr (++) []

--c
mulinceven :: [Int] -> Int
mulinceven = foldr (*) 1 . map (+ 1) . filter (>= 4)

--ex 4 -> HOF
--a
oddeven :: [(a, a)] -> [a]
oddeven xs =
  [ if i `rem` 2 == 0
    then x
    else y
  | (i, (x, y)) <- zip [0 .. length xs] xs
  ]

--b
removeRepetition :: Eq a => [a] -> [a]
removeRepetition [] = []
removeRepetition (c:cs) = c : [b | (a, b) <- zip (c : cs) cs, a /= b]

--c
sublists :: [a] -> [[a]]
sublists [] = [[]]
sublists (x:xs) = sublists xs ++ [x : subl | subl <- sublists xs]

--ex 4 -> INFINITE LISTS
--a
primes :: [Integer]
primes = sieve [2 ..]
  where
    sieve (p:xs) = p : sieve [x | x <- xs, x `mod` p > 0]

isPrime :: Integer -> Bool
isPrime n = n == head (dropWhile (< n) primes)

--b
ones = 1 : ones

evens = 0 : zipWith (+) ones odds

odds = zipWith (+) ones evens

--c
multiples :: [Integer] -> [Integer]
multiples ns = foldr merge [] [[0,x ..] | x <- ns]
  where
    merge xs [] = xs
    merge (x:xs) (y:ys)
      | x < y = x : merge xs (y : ys)
      | x > y = y : merge (x : xs) ys
      | otherwise = x : merge xs ys
