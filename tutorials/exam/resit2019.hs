length' :: [Int] -> Int
length' = foldr (\_ x -> x + 1) 0

aligned :: Eq a => [a] -> [a] -> Int
aligned xs ys =
  sum $
  zipWith
    (\a b ->
       if a == b
         then 1
         else 0)
    xs
    ys

concatMap' :: (a -> b) -> [[a]] -> [b]
concatMap' f xs = foldr (\x y -> map f x ++ y) [] xs

evenLists :: [[Int]] -> [[Int]]
evenLists xss = [filter even xs | xs <- xss]

triples :: [a] -> [a] -> [a] -> [(a, a, a)]
triples xs ys zs = [j | j <- zip3 xs ys zs]

fibs :: [Integer]
fibs = 0 : 1 : [a + b | (a, b) <- zip fibs (tail fibs)]

natLists :: [[Integer]]
natLists = helper 1
  where
    helper n = take n [0 ..] : helper (n + 1)

multiples :: [Integer] -> [Integer]
multiples xs = helper $ minimum xs
  where
    helper n
      | any (== 0) $ map (n `mod`) xs = n : helper (n + 1)
    helper n = helper (n + 1)

-- module Complex(Complex, add, sub, mul) where
data Complex =
  C Double Double

add :: Complex -> Complex -> Complex
add (C a b) (C c d) = C (a + c) (b + d)

sub :: Complex -> Complex -> Complex
sub (C a b) (C c d) = C (a - c) (b - d)

mul :: Complex -> Complex -> Complex
mul (C a b) (C c d) = C (a * c - b * d) (a * d + b * c)

listgcd xs = foldr gcd 0 xs

scanl' f b xs = b : zipWith f xs (tail xs)

powerfunc :: (a -> a) -> [a -> a]
powerfunc f = id : [f . g | g <- powerfunc f]
