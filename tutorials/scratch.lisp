(defun charToInt (s)
  (labels ((inner (idx)
             (cond ((>= idx (length s))
                    (list 1 0))
                   (t (let ((dig (digit-char-p (aref s idx))))
                        (cond (dig (destructuring-bind (base acc) (inner (1+ idx))
                                     (list (* base 10) (+ (* dig base) acc))))
                              (t (return-from charToNum 0))))))))
    (cadr (inner 0))))

(defun inits (lst) ;; O(n^2)
  (labels ((inner (lst acc)
             (if (null lst)
                 (cons '() acc)
                 (inner (butlast lst) (cons lst acc)))))
    (inner lst '())))

(defun tails (lst) (append (maplist #'(lambda (x) x) lst) '(())))

(defun subsequences (lst) ;; bad time complexity
  (labels ((inner (lst acc)
             (if (null lst)
                 acc
                 (destructuring-bind (head . tail) lst
                   (inner tail (append
                                (mapcar #'(lambda (subfix) (cons head subfix))
                                        (inits tail))
                                acc))))))
    (inner lst '())))
