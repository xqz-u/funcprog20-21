{-
ex 3.5
Give two different definitions of the nAnd function
nAnd :: Bool -> Bool -> Bool
which returns the result True except when both arguments are True
-}
nAnd :: Bool -> Bool -> Bool
nAnd a b = not (a && b)

nAnd1 :: Bool -> Bool -> Bool
nAnd1 a b = (not a) || (not b)


{-
ex 3.8
Explain the effect of the function defined here.
Answer: Return True if at least one argument is different from the others
        using transitivity.
-}
mystery :: Integer -> Integer -> Integer -> Bool
mystery m n p = not((m==n) && (n==p))


{-
ex 3.14
Give definitions of the functions min2 and minThree which calculate the minimum of
two and three integers, respectively. (Note: min is a built-in function from the
Prelude, threfore we choose the name min2)
min2 :: Int -> Int -> Int
minThree :: Int -> Int -> Int -> Int
-}
min2 :: Int -> Int -> Int
min2 x y
  | x < y = x
  | otherwise = y

minThree :: Int -> Int -> Int -> Int
minThree x y z = min2 x (min2 y z)


{-
ex 3.17
Define the function charToNum which converts a digit like ’8’ to its value 8. The
value of non-digits should be taken to be 0.
-}
import Data.Char
charToNum :: Char -> Int
charToNum x | elem x ['0'..'9'] = ord x - ord '0'
charToNum _ = 0

-- NOTE additional
toInt :: [Char] -> Int -> Int
toInt xs base = head (helper xs)
  where
    helper :: [Char] -> [Int]
    helper [] = [0, 1]
    helper (x:xs) = [(charToNum x)*b + acc, base*b]
      where
        (acc:b:_) = helper xs

toInt_def :: [Char] -> Int
toInt_def xs = toInt xs 10


{-
ex 3.22
Write a function numberNDroots that given the coefficients of the quadratic a, b
and c, will return how many (real) roots the equation has. You may assume that a
is non-zero.
-}
delta :: Float -> Float -> Float -> Float
delta a b c = b*b - 4*a*c

numberNDroots :: Float -> Float -> Float -> Integer
numberNDroots a b c
  | d > 0 = 2
  | d == 0 = 1
  | otherwise = 0
  where d = delta a b c


{-
ex 3.23
Using your answer to the last question, write a function numberRoots :: Float ->
Float -> Float -> Integer that given the coefficients of the quadratic a, b and
c, will return how many (real) roots the equation has. In the case that equation
has every number as root you should return th
-}
transitive :: Eq a => a -> a -> a -> Bool
transitive a b c = ((a == b) && (b == c))

numberRoots :: Float -> Float -> Float -> Integer
numberRoots a b c
  | transitive a b c = 3
  | otherwise = numberNDroots a b c


{-
ex 3.24
Write definitions of the functions
smallerRoot :: Float -> Float -> Float -> Float
largerRoot :: Float -> Float -> Float -> Float
which return the smaller and larger real roots of the quadratic. In the case
that the equation has no real roots or has all values as roots you should return
zero as result of each of the functions.
-}
quadraticRoot :: Float -> Float -> Float -> [Float]
quadraticRoot a b c
  | (d < 0 || (transitive a b c && a == 0)) = [0]
  | d == 0 = [b / (2*a)]
  | otherwise = [((b+sqD)/(2*a)), ((b-sqD)/(2 * a))]
  where
    d = delta a b c
    sqD = sqrt d

smallerRoot :: Float -> Float -> Float -> Float
smallerRoot a b c = foldr (min) (head roots) roots
  where
    roots = quadraticRoot a b c

largerRoot :: Float -> Float -> Float -> Float
largerRoot a b c = foldr (max) (head roots) roots
  where
    roots = quadraticRoot a b c


{-
ex 4.17
Define the function rangeProduct which when given the natural numbers m and n
returns the product m*(m+1)*...*(n-1)*n. The function should return 0 when n is
smaller than m.
-}
rangeProduct :: Integer -> Integer -> Integer
rangeProduct m n | n < m = 0
rangeProduct m n = helper m n 1
  where
    helper :: Integer -> Integer -> Integer -> Integer
    helper m n acc | m > n = acc
    helper m n acc = helper (m + 1) n (m * acc)

-- NOTE simpler
rangeProduct' :: Integer -> Integer -> Integer
rangeProduct' m n | n < m = 0
rangeProduct' m n | m == n = n
rangeProduct' m n = m * rangeProduct' (m + 1) n


{-
ex 4.18
As fac is a special case of rangeProduct, write a definition of fac which uses
rangeProduct
-}
fac :: Integer -> Integer
fac 0 = 1
fac n = rangeProduct 1 n


{-
ex 4.32
Suppose we have to raise 2 to the power n. If n is even, 2*m say, then
2^n = 2^(2*m) = (2^m)^2.
If n is odd, 2*m+1 say, then
2^n = 2^(2*m + 1) = (2^m)^2 * 2.
Give a recursive function to compute 2^n which uses these insights.
-}
sqExp :: Integer -> Integer -> Integer
sqExp b 0 = 1
sqExp b n
  | mod n 2 == 0 = acc
  | otherwise = b * acc
  where
    sq x = x * x
    acc = sq (sqExp b (div n 2))


{-
ex 5.1
Give a definition of the function
maxOccurs :: Integer -> Integer -> (Integer, Integer)
which returns the maximum of two integers, together with the number of times it
occurs. Using this, define the function
maxThreeOccurs :: Integer -> Integer -> Integer -> (Integer, Integer, Integer)
which does a similar thing for three arguments.
-}
-- NOTE don't really know what is asked here :/
-- | Return the first list element which matches a unary predicate
-- | (basically a filter: matchUnaryPredicate' f xs = [x | x <- xs, f x])
matchUnaryPredicate :: (a -> Bool) -> [a] -> [a]
matchUnaryPredicate f [] = []
matchUnaryPredicate f (x:_) | f x = [x]
matchUnaryPredicate f (_:xs) = matchUnaryPredicate f xs

maxOccurs :: Integer -> Integer -> (Integer, Integer)
maxOccurs m n | (m>0) && (n>0) = if (m > n) then (m, div m n) else (n, div n m)
maxOccurs m n
  | not (null res) = (head res, 0)
  | otherwise = (0, 0)
  where
    res = matchUnaryPredicate (\x -> x /= 0) [m, n]

maxThreeOccurs :: Integer -> Integer -> Integer -> (Integer, Integer, Integer)


{-
ex 5.18
Give a definition of the function
doubleAll :: [Integer] -> [Integer]
which doubles all the elements of a list of integers.
-}
doubleAll :: [Integer] -> [Integer]
doubleAll [] = []
doubleAll (x:xs) = (2*x):doubleAll xs

doubleAll' :: [Integer] -> [Integer]
doubleAll' xs = [2*x | x <- xs]


{-
ex 5.21
Define the function
matches :: Integer -> [Integer] -> [Integer]
which picks out all occurrences of an integer n in a list. For instance:
matches 1 [1,2,1,4,5,1]
[1,1,1]
matches 1 [2,3,4,6]
[]
Next, use it to implement the function isElementOf n xs which returns True if n
occurs in the list xs, and False otherwise.
-}
matches :: Integer -> [Integer] -> [Integer]
matches n xs = helper n xs []
  where
    helper :: Integer -> [Integer] -> [Integer] -> [Integer]
    helper _ [] acc = acc
    helper n (x:xs) acc | x == n = helper n xs (x:acc)
    helper n (_:xs) acc = helper n xs acc

matches' :: Integer -> [Integer] -> [Integer]
matches' n xs = [x | x <- xs, x == n]

isElementOf :: Integer -> [Integer] -> Bool
isElementOf n xs = not (null (matches n xs))
