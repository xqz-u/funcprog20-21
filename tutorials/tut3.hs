import Data.Ord

-- ex 11.8
total :: (Integer -> Integer) -> (Integer -> Integer)
total f = \n -> sum [f x | x <- [0 .. n]]

-- ex 11.9+10
flip' :: (a -> b -> c) -> (b -> a -> c) -- return is actually a -> b -> c
flip' f = \b a -> f a b

flip'' :: (a -> b -> c) -> b -> a -> c
flip'' f a b = f b a

-- NOTE useful exampl
-- map ($ 3) [(4+), (10*), (^2), sqrt] = [7.0,30.0,9.0,1.7320508075688772]
-- ex 11.14
dummyC x y = ((*) $ x) $ y -- example usage function application ($)

-- uncurry :: (a -> b -> c) -> ((a, b) -> c)
{-
($) :: (a -> b) -> a -> b
uncurry ($) :: ((a -> b), a) -> b
e.g. ($) ((*) 2) 5 == uncurry ($) ((*), 2) 5
NOTE the uncurried version does not work on partially applied functions, e.g.
(* 2), since an argument already bound cannot be bundled in a tuple by uncurry!
-}
{-
(:) :: a -> [a] -> [a]
uncurry (:) :: (a, [a]) -> [a]
e.g. (:) 5 [] == uncurry (:) (5, [])
-}
{-
(.) -> (b -> c) -> (a -> b) -> a -> c
uncurry (.) :: ((b -> c), (a -> b)) -> a -> c
e.g. (.) (+ 2) (* 3) 4 == uncurry (.) ((+ 2), (* 3)) 4
-}
-- ex 11.15
{-
1. uncurry uncurry :: ((a -> b -> c), (a, b)) -> c
Basically bundles together in a single tuple two arguments of any type
e.g. (+) 1 2 == uncurry (+) (1, 2) == uncurry uncurry ((+), (1, 2))

2. curry uncurry :: ((a, b) -> c, a) -> b -> c
e.g. ???
-}
-- ex 11.17
dummy = \a b c -> a + b + c

dummy1 = \(a, b, c) -> a + b + c

curry3 :: ((a, b, c) -> d) -> (a -> b -> c -> d)
curry3 = \f x y z -> f (x, y, z) -- NOTE all in point free style

uncurry3 :: (a -> b -> c -> d) -> ((a, b, c) -> d)
uncurry3 = \f (x, y, z) -> f x y z

-- curry3' = \f x y z -> ??? TODO
uncurry3' = \f (x, y, z) -> uncurry f (x, y) z -- can add $ before z

-- NOTE examples function composition / point-free style
expression =
  replicate
    100
    (product (map (* 3) (zipWith max [1, 2, 3, 4, 5] [4, 5, 6, 7, 8])))

expression' =
  replicate 100 . product . map (* 3) . zipWith max [1, 2, 3, 4, 5] $
  [4, 5, 6, 7, 8]

fn x = ceiling (negate (tan (cos (max 50 x))))

fn' = ceiling . negate . tan . cos . max 50

-- ex 11.19
iter :: Integer -> (a -> a) -> (a -> a)
iter n f
  | n > 0 = f . iter (n - 1) f
  | otherwise = id

double :: Num a => a -> a
double x = 2 * x

add :: Num a => a -> a -> a
add x y = x + y

sq :: Num a => a -> a
sq x = x * x

succ' :: Integer -> Integer
succ' n = n + 1

comp2 :: (a -> b) -> (b -> b -> c) -> (a -> a -> c)
comp2 f g = (\x y -> g (f x) (f y))

{-
iter 3 double 1 =
= double . iter 2 double 1 =
= double . (double . iter 1 double 1) =
= double . (double . (double . iter 0 double 1)) =
= double . (double . ((double . id) 1)) =
= double . (double . (double (id 1))) =
= double . (double . (double 1)) =
= double . (double . (2 * 1)) =
= ... = 8 ?

= double . iter 2 double 1
= double (iter 2 double 1) =
= 2 * (iter 2 double 1) =
= 2 * (double . iter 1 double 1) =
= 2 * (double (iter 1 double 1)) =
= 2 * (2 * (iter 1 double 1)) =
= 2 * (2 * (double . iter 0 double 1)) =
= 2 * (2 * (2 * (iter 0 double 1))) =
= 2 * (2 * (2 * (id 1))) =
= 2 * (2 * (2 * 1)) = 8

(comp2 succ' (*)) 3 4 =
= (\x y -> (*) (succ' x) (succ' 4)) 3 4 =
= (*) (succ' 3) (succ' 4) =
= (*) (3 + 1) (succ' 4) =
= (*) 4 (succ' 4) =
= (*) 4 (4 + 1) =
= (*) 4 5 = 20

comp2 sq add 3 4 =
= (\x y -> add (sq x) (sq y)) 3 4
= add (sq 3) (sq 4) =
= (sq 3) + (sq 4) =
= (3 * 3) + (sq 4) =
= 9 + (sq 4) =
= 9 + (4 * 4) =
= 9 + 16 = 25
-}
-- ex 11.20
ex :: Integer -> Integer -> Integer
ex = \n -> iter n succ' -- perform succ' (partially applied) n times

-- ex 11.2
iter' :: Integer -> (a -> a) -> (a -> a)
iter' n f = foldr (.) id [f | x <- [1 .. n]]

-- ex 12.13
splits :: [a] -> [([a], [a])]
splits xs = map (\n -> (take n xs, drop n xs)) [0 .. length xs]

-- ex 17.2
sublists :: [a] -> [[a]]
sublists [] = [[]]
sublists (x:xs) = [x : subl | subl <- sublists xs] ++ sublists xs --from Stack :/

subsequences :: [a] -> [[a]]
subsequences [] = [[]]
subsequences (x:xs) = [x : subfix | subfix <- subsequences xs] -- TODO

-- ex 17.23
factorial :: [Integer]
factorial = factorials [0 ..]
  where
    factorials (x:xs) = foldr (*) 1 [1 .. x] : factorials xs

factorial' = 1 : zipWith (*) [1 ..] factorial' -- better memory usage

fibonacci :: [Integer]
fibonacci = 0 : 1 : zipWith (+) fibonacci (tail fibonacci)

-- ex 17.24
factors :: Integer -> [Integer]
factors n = 1 : [x | x <- [2 .. n `div` 2], n `mod` x == 0] ++ [n]

prime :: Integer -> Bool
-- | Check if an Integer n is prime
prime n -- 2 and 3 are primes
  | n <= 3 = n > 1
prime n -- Even numbers are not prime
  | mod n 2 == 0 = False
prime n = helper 3 n -- Check divisibility by odd numbers up to m's square root
  where
    helper m n
      | m * m > n = True
      | mod n m == 0 = False
      | otherwise = helper (m + 2) n

hamming :: [Integer] -- NOTE not efficient, generative approach is better
hamming = 2 : 3 : [h | h <- [4 ..], all (< 7) . filter prime . factors $ h]

-- One liner from https://wiki.haskell.org/Blow_your_mind, check memory usage
-- h = 1 : unionAll [map (k *) h | k <- [2, 3, 5]]
-- ex 17.25
runningSums :: [Int] -> [Int]
runningSums xs = 0 : zipWith (+) xs (runningSums xs)
-- ex 17.29 TODO
