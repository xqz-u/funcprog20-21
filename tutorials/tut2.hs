import Data.Char

-- ex 7.2
firstEx :: [Int] -> Int
firstEx (x:y:_) = x + y
firstEx (x:_) = x
firstEx _ = 0

-- ex 7.6
and2 :: [Bool] -> Bool
and2 xs = foldr (&&) True xs

or2 :: [Bool] -> Bool
or2 xs = foldr (||) False xs

-- ex 7.8
elemNum :: Integer -> [Integer] -> Integer
elemNum _ [] = 0
elemNum n (x:xs)
  | n == x = 1 + elemNum n xs
  | otherwise = elemNum n xs

elemNum' :: Integer -> [Integer] -> Integer
elemNum' n xs = sum [1 | x <- xs, n == x]

-- ex 7.9
unique :: [Integer] -> [Integer]
unique xs = helper xs xs []
  where
    helper [] _ acc = reverse acc
    helper (x:xs) xss acc
      | elemNum x xss == 1 = helper xs xss (x : acc)
      | otherwise = helper xs xss acc

unique' :: [Integer] -> [Integer]
unique' xs = [x | x <- xs, elemNum x xs == 1]

-- ex 7.16
iSortGen :: (Integer -> [Integer] -> [Integer]) -> [Integer] -> [Integer]
iSortGen _ [] = []
iSortGen insFunc (x:xs) = insFunc x (iSortGen insFunc xs)

decrIns :: Integer -> [Integer] -> [Integer]
decrIns x [] = [x]
decrIns x (y:ys)
  | x >= y = x : y : ys
  | otherwise = y : decrIns x ys

insUnique :: Integer -> [Integer] -> [Integer]
insUnique x [] = [x]
insUnique x (y:ys)
  | x < y = x : y : ys
  | x == y = insUnique x ys
  | otherwise = y : insUnique x ys

-- ex 7.25
subListSeq :: String -> String -> String
-- | Naive implementation: sublist has precedence over subsequence,
-- | moreover only one match is supported (the function will deal with the first
-- | match found)
subListSeq p s = helper p s 0 0
  where
    helper :: String -> String -> Int -> Int -> String
    helper [] _ pos start --first match of p in s, possibly non exhausted
      | pos - start == length p = "subsequence"
      | otherwise = "sublist"
    helper (p:ps) (s:ss) pos start
      | p == s && start == 0 = helper ps ss (pos + 1) pos --init start
      | p == s = helper ps ss (pos + 1) start --general positive match
      | otherwise = helper (p : ps) ss (pos + 1) start --negative match
    helper _ [] _ _ = "nothing" --all other cases

-- ex 7.33
lowerAlphanum :: String -> String
-- onlyAlphanum xs = filter (\x -> (not . isPunctuation) x) xs
lowerAlphanum xs = [toLower x | x <- xs, not $ isPunctuation x || isSpace x]

palin :: String -> Bool
palin xs
  | even . length $ xs = ((==) . (reverse . drop len)) xs fhalf
  | otherwise = ((==) . (reverse . (drop . (+) 1) len)) xs fhalf
  where
    len = (div . length) xs 2
    fhalf = take len xs

isPalin :: String -> Bool
isPalin xs = palin . lowerAlphanum $ xs

-- ex 7.34
subst :: String -> String -> String -> String
subst old new s = helper old s [] []
  where
    helper [] s _ sAcc = reverse sAcc ++ new ++ s -- first positive match
    helper _ [] _ sAcc = reverse sAcc -- negative match, og string ended
    helper (p:ps) (s:ss) pAcc sAcc
      -- accumulate in pattern buffer during partial positive match
      | p == s = helper ps ss (p : pAcc) sAcc
      -- no match has started yet, accumulate string scanned so far
      | null pAcc = helper (p : ps) ss pAcc (s : sAcc)
      -- negative match while a partial match started
      | otherwise = helper old ss [] (s : pAcc ++ sAcc)
