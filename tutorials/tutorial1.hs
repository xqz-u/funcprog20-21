nAnd :: Bool -> Bool -> Bool

nAnd True True = False
nAnd _ _ = True

min2 :: Int -> Int -> Int
min22 :: Int -> Int -> Int
min3 :: Int -> Int -> Int -> Int

min2 a b = if a < b then a else b
min22 a b
    | a < b = a
    | otherwise = b

min3 a b c = min2 a (min2 b c)

rangeProduct :: Int -> Int -> Int
rangeProduct2 :: Int -> Int -> Int

rangeProduct m n
    | n < m = 0
    | otherwise = product [m..n]

rangeProduct2 m n
    |n < m = 0
    |n == m = n
    |otherwise = m * rangeProduct2 m n-1

power2 :: Int -> Int

power2 n
    |mod n 2 == 0 = (2 ^ (div n 2)) ^ 2
    |otherwise = ((2 ^ (div (n-1) 2)) ^ 2) * 2

power22 n
    |n == 0 = 1
    |even n = power22 (div n 2) ^ 2
    |otherwise = 2 * power22 (n-1)

maxOccurs :: Integer -> Integer -> (Integer, Integer)
maxOccurs x y
    |x == y = (x,2)
    |x < y = (y,1)
    |otherwise = (x,1)

max3Occurs :: Integer -> Integer -> Integer -> (Integer, Integer)
max3Occurs x y z
    |z == m = (z, numOccurs +1)
    |z < m = (m, numOccurs)
    |otherwise = (z, 1)
        where(m, numOccurs) = maxOccurs x y

doubleAll :: [Integer] -> [Integer]
doubleAll xs = [2*x | x <- xs]

match :: Integer -> [Integer] -> [Integer]
match n xs = [x | x <- xs, x == n]
